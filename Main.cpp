#include <cstdio>
#include <cstdlib>
#include <conio.h>
#include <ctime>
#include <windows.h>

#define cls() system("CLS")
#define SIZE 60
#define HSIZE 60
#define RSIZE 4
#define ROOMS 15+15
#define SCORE 50+50
#define BUFF 25
#define MAXROOMS 150
#define MIN(x,y) ((x>y)?(y):(x))
#define MAX(x,y) ((x>y)?(x):(y))
#define AVG(x,y) ((x+y)/2)
#define RESETMAZE 600
#define Vision 15
//150% for tecnicle proposes (reduction occurs after taking the buff)
#define MAXENERGY 151
#define MAXLENERGY 100

void wait()
{
	for(long a=0;a<16000;a++)
		for(long b=0;b<8000;b++);
}
void gotoxy( int x, int y )
{
    COORD p = { x, y };
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), p );
}
class AppGame
{
	void maze();
	void print();
	int game();
	void init();
	void GameOver();
	bool ifEmpty(int,int,int,int);
	void makeRoom(int,int,int,int,int);
	int Maze[SIZE][HSIZE];
	int px,py,score,level;bool win;
	bool isEmpty(int,int,bool=false);
	void fixDoors();
	int canPass(int,int);
	void start();
	void loop()
	{
		init();
		while(1)
		{
			//cls();
			gotoxy(0,0);
			_flushall();
			print();
			if(game()) break;
		}
		if(win)
		{
			loop();
		}
		/*
		if(win)
		{
			cls();
			_flushall();
			printf("Game - Maze(Tutorial) Level: %d\nScore:%-4d",level,score);
			FILE *f;
			if(fopen_s(&f,"score.scr","r"))
			{
				fopen_s(&f,"score.scr","w");
				fprintf(f,"%d",score);
				printf("\nNew Highest score!!!");
			}
			else
			{
				int best;
				fscanf_s(f,"%d",&best);
				if(best==score)
				{
					printf("\nHighest score!!");
				}
				else if(best<score)
				{
					printf("\nNew Highest score!!!");
					fclose(f);
					fopen_s(&f,"score.scr","w");
					fprintf(f,"%d",score);
				}
				else
				{
					printf("\nHighest score: %d!", best);
				}
			}
			fclose(f);
			printf("\nYou Wim the Game.");
			_getch();//
			//return 1;
		}*/
	}
public:
	int main()
	{
		start();
		loop();
		
		return 0;
	}
};
enum {FLOOR,PLAYER,EXIT,ODOOR,CDOOR,ENERGY,WALL,BG};
void main()
{

	system("color 1E");
	AppGame Game;
	while(Game.main());
}
void AppGame::start()
{
	score=SCORE*0;
	level=0;
}
void AppGame::maze()
{
	for(int i=0;i<SIZE;i++)
		for(int j=0;j<HSIZE;j++)
		{
			Maze[i][j]=0;
			if((i==0)||(j==0)||(i==(SIZE-1))||(j==(HSIZE-1)))
				Maze[i][j]='w';
			else
				Maze[i][j]='f';
		};//Generate an empty Room
	;//Add player and other stuff
	int k=MIN(ROOMS*level+rand()%2,MAXROOMS);
	//(MAXROOMS-k)/(0*ROOMS)
	int roomSize=RSIZE+MAX((MAXROOMS-(ROOMS*level))/(0*ROOMS),0);
#ifdef _DEBUG
	printf("rooms: %d\nmax Size: %d\n",k,roomSize);
#endif
	printf(".");
	int test=0;
	while(k>0)
	{
		int sizeX = 3+rand()%(roomSize);
		int sizeY = 3+rand()%(roomSize);
		int x = rand()%(SIZE-sizeX);
		int y = rand()%(HSIZE-sizeY);
		if(ifEmpty(x,y,sizeX,sizeY))
		{
			int doors = 0;
			if((sizeX+sizeY-5)>0)
			{
				doors = 2+rand()%((sizeX+sizeY-5));
			}
			makeRoom(x,y,sizeX,sizeY,doors);
			k--;
			test=0;
		}
		test++;
		if(test>RESETMAZE)
		{
			maze();
			return;
		}
	}
#ifdef _DEBUG
	_flushall();
	//_getch();
	wait();
	cls();
#endif
}
void AppGame::init()
{
	printf("Loading");
	win=false;level++;
	score+=0*SCORE;
	if(score>MAXLENERGY)
		score=MAXLENERGY;
	srand(time(NULL));
	maze();
	bool f=true;
	while(f)
	{
		for(int i=1;f&&i<SIZE-1;i++)
		{
			for(int j=1;f&&j<HSIZE-1;j++)
			{
				if((Maze[i][j]=='f')
					&&(rand()%6==3))
				{
					px=i;py=j;
					Maze[i][j]='p';
					f=false;
				}
			}
		}
	}
	printf(".");
	
	fixDoors();
	f=true;
	while(f)
	{
		for(int i=SIZE-1;f&&i>1;i--)
		{
			for(int j=HSIZE-1;f&&j>1;j--)
			{
				if((Maze[i][j]=='f')
					&&(rand()%6==3))
				{
					Maze[i][j]='e';
					f=false;
				}
			}
		}
	}
	printf(".");
	cls();
	_flushall();
}
int AppGame::game()
{
	
	char get=_getch();
	if((get=='q')||(get=='Q')) return 1;
	int dx=0;int dy=0;
	switch(get)
	{
	case 'q':
	case 'Q':
		return 1;
	case 'w':
	case 'W':
		dx=px-1;
		dy=py;
		break;
	case 'a':
	case 'A':
		dx=px;
		dy=py-1;
		break;
	case 's':
	case 'S':
		dx=px+1;
		dy=py;
		break;
	case 'd':
	case 'D':
		dx=px;
		dy=py+1;
		break;
	default:
		;
	}
	if(dx*dy)
	{
		if(Maze[dx][dy]=='e')
		{
			win=true;
			return 1;
		}
		if(isEmpty(dx,dy))
		{
			if(Maze[px][py]=='P')
			{
				Maze[px][py]='o';
			}
			else
				Maze[px][py]='f';
			if(Maze[dx][dy]=='c')
			{
				Maze[dx][dy]='p';
				score+= BUFF;
				if(score>MAXENERGY)
					score=MAXENERGY;
			}
			else if(Maze[dx][dy]=='f')
			{
				Maze[dx][dy]='p';
			}
			else
				Maze[dx][dy]='P';
			px=dx;py=dy;
		}
		score--;
		if(score==0)
		{
			GameOver();
			
			char ch=0;
			while(ch!='y')
			{
				ch=_getch();
				if(ch=='n')
					return 1;
			}
			start();
			init();
			return 0;
		}
	}
	return 0;
}
void AppGame::print()
{
	const WORD colors[] =
		{//0X(BG)(TXT)
		/*
		0-BLACK
		1-BLUE
		2-GREEN
		3-COLA
		4-RED
		5-PURPLE
		6-YELLOW
		7-GREY
		*/
		0x2A,//floor
		0x74,//player
		0x96,
		0xE0,
		0x60,
		0xC4,//energy
		0x87,//wall


		0x9E,
		0x1E, 0xB2, 0xC3, 0xD4, 0xE5, 0xF6
		};

	HANDLE hstdin  = GetStdHandle( STD_INPUT_HANDLE  );
	HANDLE hstdout = GetStdHandle( STD_OUTPUT_HANDLE );
	//WORD   index   = 0;
	// Remember how things were when we started
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo( hstdout, &csbi );
	gotoxy(0,1);
	for(int i=0;i<Vision*2+3;i++)
	{
		SetConsoleTextAttribute( hstdout, colors[ BG ] );
		printf("  ");
	}
	gotoxy(0,0);
	//system("color 1E");
#ifdef _DEBUG
	
	SetConsoleTextAttribute( hstdout, colors[ BG + 1 ] );
	printf("Game - Maze(DeBug-Mode) Level: %d\n",level);
	SetConsoleTextAttribute( hstdout, colors[ BG ] );
	printf("Energy:%3d%c Chest Chance 1:%d times %d to 1\n",score,'%',MIN(2+MAX(((level)/2),0),20),1+(MAXROOMS-(ROOMS*level))/(0*ROOMS));
#else
	SetConsoleTextAttribute( hstdout, colors[ BG + 1 ] );
	printf("              Game - Maze(v1.0) Level: %d\n",level);
	SetConsoleTextAttribute( hstdout, colors[ BG ] );
	printf("    Energy:%3d%c\n",score,'%');
#endif

	//printf("\n");
	for(int i=0;i<SIZE;i++)
	{
		
		if((px-Vision<0))
		{
			if(i>(Vision*2)) continue;
			
		}
		else if((SIZE-px)<=(Vision))
		{
			if(i<=SIZE-(Vision*2+2))
				continue;
		}
		else
		{
			if((i<px-Vision)||(i>px+Vision)) continue;
		}
		SetConsoleTextAttribute( hstdout, colors[ BG ] );
		printf("  ");
		for(int j=0;j<HSIZE;j++)
		{
			if(py-Vision<0)
			{
				if(j>(Vision*2)) continue;
			}
			else if((HSIZE-py)<=(Vision))
			{
				if(j<=HSIZE-(Vision*2+2)) continue;
			}
			else
			{
				if((j<py-Vision)||(j>py+Vision)) continue;
			}
			switch(Maze[i][j])
			{
			case 'f':
				SetConsoleTextAttribute( hstdout, colors[ FLOOR ] );
				printf("..");
				break;
			case 'p':
			case 'P':
				SetConsoleTextAttribute( hstdout, colors[ PLAYER ] );
				printf("><");
				break;
			case 'e':
				SetConsoleTextAttribute( hstdout, colors[ EXIT ] );
				printf("88");
				break;
			case 'o':
				SetConsoleTextAttribute( hstdout, colors[ ODOOR ] );
				printf(". ");
				break;
#ifdef _DEBUG
			case 'd':
				SetConsoleTextAttribute( hstdout, colors[ CDOOR ] );
				printf(". ");
				break;
#endif
			case 'c':
				SetConsoleTextAttribute( hstdout, colors[ ENERGY ] );
				printf("qp");
				break;
			case 'w':
			default:
				SetConsoleTextAttribute( hstdout, colors[ WALL ] );
				printf("[]");
				break;
			}
		}
		SetConsoleTextAttribute( hstdout, colors[ BG ] );
		printf("  ");
		printf("\n");
	}
	for(int i=0;i<Vision*2+3;i++)
	{
		SetConsoleTextAttribute( hstdout, colors[ BG ] );
		printf("  ");
	}
	printf("\n");
	FlushConsoleInputBuffer( hstdin );
	// Keep users happy
	SetConsoleTextAttribute( hstdout, csbi.wAttributes );

}
bool AppGame::ifEmpty(int x,int y,int toX,int toY)
{
	if((x+toX)>SIZE) return false;
	if((y+toY)>HSIZE) return false;
	for(int i=0;i<toX;i++)
	{
		for(int j=0;j<toY;j++)
		{
			if((i!=0)&&(i!=toX-1))
			{
				if((j!=0)&&(j!=toY-1))
					continue;
			}
			if(Maze[x+i][y+j]!='f')
				return false;
		}
	}
	return true;
}
void AppGame::makeRoom(int x,int y,int sizeX,int sizeY,int doors)
{
	for(int i=0;i<sizeX;i++)
	{
		Maze[x+i][y]='w';
		Maze[x+i][y+sizeY-1]='w';
	}
	if(sizeY>2)
		for(int i=0;i<(sizeY-2);i++)
		{
			Maze[x][y+i+1]='w';
			Maze[x+sizeX-1][y+i+1]='w';
		};
	for(int i=0;i<AVG(sizeX-2,sizeY-2);i++)
	{//make chest
		if(rand()%MIN(2+MAX(((level)/2),0),20)==0)
			Maze[x+1+rand()%(sizeX-2)][y+1+rand()%(sizeY-2)]='c';
	}
	for(int i=0;i<doors;i++)
	{
		bool d=false;
		while(!d)
		{
			int edge = rand()%4;int side = edge/2;
			for(int jk=0;(jk<4)&&(!d);jk++)
			{
				edge=(edge+1)%4;
				side = edge/2;
				int inital,pos;
				if(edge%2)
				{
					inital=rand()%(sizeX-2);
					for(int j=1;j<(sizeX-1);j++)
					{
						pos=1+((j+inital-1)%(sizeX-2));
						if(
							(!isEmpty(
								x+pos,
								y+(sizeY-1)*side))
							&&isEmpty(
								x+pos,
								y+(sizeY-1)*side-1)
							&&isEmpty(
								x+pos,
								y+(sizeY-1)*side+1)
							)
						{
							Maze[x+pos][y+(sizeY-1)*side]='d';
							d=true;
							break;
						}
					}
				}
				else
				{
					inital=rand()%(sizeY-2);
					for(int j=1;j<(sizeY-1);j++)
					{
						pos=1+((j+inital-1)%(sizeY-2));
						if(
							(!isEmpty(
								x+(sizeX-1)*side,
								y+pos))
							&&isEmpty(
								x+(sizeX-1)*side+1,
								y+pos)
							&&isEmpty(
								x+(sizeX-1)*side-1,
								y+pos)
							)
						{
							Maze[x+(sizeX-1)*side][y+pos]='d';
							d=true;
							break;
						}
					}
				}
			}
			if(!d)
			{
				Maze[x][y]='d';
				Maze[x+(sizeX-1)][y]='d';
				Maze[x][y+(sizeY-1)]='d';
				Maze[x+(sizeX-1)][y+(sizeY-1)]='d';
				d=true;
			}
		}
	}
}
bool AppGame::isEmpty(int x,int y,bool all)
{
	if(all&&Maze[x][y]!='f') return false;
	if(Maze[x][y]=='w') return false;
	return true;
}
void AppGame::fixDoors()
{
	for(int i=1;i<SIZE-2;i++)
	{
		for(int j=1;j<HSIZE-2;j++)
		{
			if(Maze[i][j]!='d') continue;
			switch(canPass(i,j))
			{
			case 0:
			case 2:
				if(!isEmpty(i,j+1))
					Maze[i][j+1]='d';
				if(!isEmpty(i,j-1))
					Maze[i][j-1]='d';
				break;
			case 1:
			case 3:
				if(!isEmpty(i+1,j))
					Maze[i+1][j]='d';
				if(!isEmpty(i-1,j))
					Maze[i-1][j]='d';
				break;
			case (-1):
			case (-2):
			default:
				break;
			}
		}
	}
}
int AppGame::canPass(int x,int y)
{
	if((isEmpty(x,y-1)&&isEmpty(x,y+1))||(isEmpty(x-1,y)&&isEmpty(x+1,y)))
		return -1;
	if(!isEmpty(x,y-1)&&isEmpty(x,y+1,true))
		return 0;
	if(isEmpty(x,y-1,true)&&!isEmpty(x,y+1))
		return 2;

	if(!isEmpty(x-1,y)&&isEmpty(x+1,y,true))
		return 1;
	if(isEmpty(x-1,y,true)&&!isEmpty(x+1,y))
		return 3;

	if(!isEmpty(x,y-1)&&isEmpty(x,y+1))
		return 0;
	if(!isEmpty(x-1,y)&&isEmpty(x+1,y))
		return 1;
	if(isEmpty(x,y-1)&&!isEmpty(x,y+1))
		return 2;
	if(isEmpty(x-1,y)&&!isEmpty(x+1,y))
		return 3;
	return -2;
}
void AppGame::GameOver()
{
	
	cls();
	_flushall();
	printf("Game - Maze(v1.0)\nLevel:%d!\n~~~Game Over~~~\n",level);
	printf("...\nfor some reason you didn't make it out of the maze in time\nyou run out of Energy and Died..\nSadly you can try again\n\nbut the odds are not in your favore...\n              -Dimanari TM");
	
	FILE *f;
			if(fopen_s(&f,"Level.scr","r"))
			{
				fopen_s(&f,"Level.scr","w");
				fprintf(f,"%d",level);
				printf("\nNew Highest Level!!!");
			}
			else
			{
				int best;
				fscanf_s(f,"%d",&best);
				if(best==level)
				{
					printf("\nHighest Level!!");
				}
				else if(best<level)
				{
					printf("\nNew Highest Level!!!");
					fclose(f);
					fopen_s(&f,"Level.scr","w");
					fprintf(f,"%d",level);
				}
				else
				{
					printf("\nHighest Level: %d!", best);
				}
			}
			fclose(f);
	
	printf("\nPlay Again?");
}